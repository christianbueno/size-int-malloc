#include <stdio.h>
#include <iostream>

using namespace std;

void passByRef(int & ref);
void passByPtr(int * ptr);

int main()
{
	int x = -5, y = 0xff;
	int* xptr;
	
	printf("(i)x = %i\n", x);
	printf("(d)x=%d\n", x);
	printf("(i)hexadecimal 0xff  %i\n", 0xff);
	printf("(i)octal 010 %i\n", 010);
	printf("(d)octal 010 %d\n", 010);
	printf("(i)hex 0x12 %i\n", 0x12);
	printf("(d)hex 0x12 %d\n", 0x12);

	printf("\nEnter a (d) number: ");
	scanf("%d", &x);
	printf("Enter a (i) number: ");
	scanf("%i", &y);
	printf("\n(i)x = %i, (i)y = %i\n", x, y);
	
	cout << "\nPass by ref" << endl;
	cout << "x = " << x << endl;
	passByRef(x);
	cout << "x = " << x << endl;
	cout << "\nPass by pointer" << endl;
	xptr = &x;
	cout << "x = " << x << endl;
	cout << "xptr = " << *xptr << endl;
	passByPtr(xptr);
	cout << "x = " << x << endl;
	cout << "xptr = " << *xptr << endl;

	
	return 0;
}

void passByRef(int & ref)
{
	ref = 20;
	printf("ref = %i\n", ref);
}
void passByPtr(int * ptr)
{
	*ptr = 30;
	printf("*ptr = %i\n", *ptr);
}
