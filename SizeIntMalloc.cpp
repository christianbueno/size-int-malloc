#include <iostream>
using namespace std;

int main() {
//	int SomeArray[10];
//	int *pLocation6 = &SomeArray[6];
//	int *pLocation0 = &SomeArray[0];

//	cout << "pLocation6 = " << pLocation6 << endl;
//	cout << "pLocation0 = " << pLocation0 << endl;
//	cout << "Difference = " << pLocation6 - pLocation0 << endl;

	cout << "hello, learning ponters in cpp" << endl;
	int a;
	int total=0;
	int *p;
	p = &a;
	a = 5;
	
	cout << "p addres: " << p << endl;
	cout << "p value: " << *p << endl;
	cout << "a address: " << &a << endl;
	cout << "a value: " << a << endl;

	int luckyNumbers[5] = {2,3,5,7,9};
	cout << luckyNumbers << endl;
	cout << &luckyNumbers[0] << endl;
	cout << &luckyNumbers[2] << endl;
	cout << sizeof(int) << " " << sizeof(char) << " " << sizeof(long) << endl;
	cout << *(luckyNumbers + 2) << endl;
	cout << luckyNumbers[2] << endl;
	for (int i=0; i<5; i++){
		cout << "Number:";
		cin >> luckyNumbers[i];
		total+= luckyNumbers[i];
	}
	cout << "Total is:" << total << endl;
	int& ref = a;
	cout << "ref: " << ref << ", a: " << a << endl;
	cout << "address ref: " << &ref << endl;

	system("pause>0");
	return 0;
//	cout << endl; system("pause");

}
